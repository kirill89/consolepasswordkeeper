var fs = require('fs');
var XXTEA = require('./XXTEA');

var dbFileName = 'data.db';
var dbData = null;
var onReady = null;
var userPassword = null;

function load() {
    fs.readFile(dbFileName, 'utf8', function (err, data) {
        if (err) {
            if (err.code == 'ENOENT') {
                dbData = {};
                dbData.checkword = 'check word';
                dbData.passwords = [];
                save(onReady);
            } else {
                console.log(err);
            }
        } else {
            try {
                dbData = JSON.parse(XXTEA.decrypt(data, userPassword));
                try {
                    if (dbData.checkword != 'check word') {
                        throw new Error();
                    }
                    onReady();
                } catch (err) {
                    console.log('password incorrect');
                }
            } catch (err) {
                console.log(err);
            }
        }
    });
}

function save(callback) {
    fs.writeFile(dbFileName, XXTEA.encrypt(JSON.stringify(dbData), userPassword), function (err) {
        if (err) {
            console.log(err);
        } else {
            if (callback != undefined) {
                callback();
            }
        }
    });
}

function get() {
    return dbData;
}

this.save = save;
this.start = function (password, readyEvent) {
    userPassword = password;
    onReady = readyEvent;
    load();
}
this.get = get;