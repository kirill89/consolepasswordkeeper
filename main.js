var db = require('./db');
var prompt = require('prompt');

function showMenu() {
    var schema = [
        {name: 'action', message: '[q]uit, [a]dd, [l]ist, [s]how, [r]emove'}
    ];

    prompt.get(schema, function (error, result) {
        if (error) {
            console.log(error);
            return;
        }
        if (result.action == 'quit' || result.action == 'q') {
            db.save(function () {
                console.log('bye!');
            });
        } else if (result.action == 'add' || result.action == 'a') {
            add();
        } else if (result.action == 'list' || result.action == 'l') {
            list();
        } else if (result.action == 'show' || result.action == 's') {
            show();
        } else if (result.action == 'remove' || result.action == 'r') {
            remove();
        } else {
            showMenu();
        }
    });
}

function add() {
    var schema = [
        {name: 'title'},
        {name: 'login'},
        {name: 'password', hidden: true},
        {name: 'repeat', hidden: true}
    ];

    prompt.get(schema, function (error, result) {
        if (error) {
            console.log(error);
            return;
        }

        if (result.password != result.repeat) {
            console.log('password and repeat is not equals');
            add();
        } else {
            db.get().passwords.push({
                title: result.title,
                login: result.login,
                password: result.password
            });
            console.log('ok');
            showMenu();
        }
    });
}

function list() {
    var passwords = db.get().passwords;
    for (var i = 0; i < passwords.length; i++) {
        console.log(passwords[i].title);
    }
    showMenu();
}

function show() {
    var schema = [
        {name: 'title'}
    ];
    prompt.get(schema, function (error, result) {
        if (error) {
            console.log(error);
            return;
        }

        if (result.password != result.repeat) {
            console.log('password and repeat is not equals');
            add();
        } else {
            var passwords = db.get().passwords;
            var found = false;
            for (var i = 0; i < passwords.length; i++) {
                if (passwords[i].title == result.title) {
                    found = true;
                    console.log(
                        '\ntitle:\n\n' + passwords[i].title + '\n\nlogin:\n\n' + passwords[i].login + '\n\npassword:\n\n' + passwords[i].password + '\n'
                    );
                    break;
                }
            }
            if (!found) {
                console.log('not found');
            }
            showMenu();
        }
    });
}

function remove() {
    var schema = [
        {name: 'title'}
    ];
    prompt.get(schema, function (error, result) {
        if (error) {
            console.log(error);
            return;
        }

        if (result.password != result.repeat) {
            console.log('password and repeat is not equals');
            add();
        } else {
            var passwords = db.get().passwords;
            var found = false;
            for (var i = 0; i < passwords.length; i++) {
                if (passwords[i].title == result.title) {
                    found = true;
                    db.get().passwords.splice(i, 1);
                    console.log('ok');
                    break;
                }
            }
            if (!found) {
                console.log('not found');
            }
            showMenu();
        }
    });
}

function onPasswordGet(password) {
    db.start(password, function () {
        showMenu();
    });
}

function requestPassword(isFirstRequest) {
    var schema = [
        {name: 'password', hidden: true}
    ];
    if (isFirstRequest === true) {
        schema.push(
            {name: 'repeat', hidden: true}
        );
    }

    prompt.get(schema, function (error, result) {
        if (error) {
            console.log(error);
            return;
        }
        if (isFirstRequest === true) {
            if (result.password != result.repeat) {
                console.log('password and repeat is not equals');
                requestPassword(true);
            }
        }
        onPasswordGet(result.password)
    });
}

prompt.start();

requestPassword(false);